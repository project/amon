# Amon integration for Drupal

This module provides a watchdog implementation that sends all watchdog messages
and exceptions to an Amon server.  

## Installation

See http://amon.cx/#install for information on installing Amon.

Enable this module as usual.  In your settings.php file, provide the url to your
Amon service.  This defaults to "http://127.0.0.1:2464/", the Amon service
default.  Make sure to include the trailing slash.

    $conf['amon_url'] = 'http://127.0.0.1:2464/';
